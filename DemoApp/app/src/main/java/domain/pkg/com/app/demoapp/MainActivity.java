package domain.pkg.com.app.demoapp;

import android.annotation.SuppressLint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    EditText firstNumberTxt;
    EditText secoundNumberTxt;
    Button addButton;
    TextView result;

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        firstNumberTxt = (EditText) findViewById(R.id.textFisrtNumber);
        secoundNumberTxt = (EditText) findViewById(R.id.textSecoundNumber);
        addButton = (Button) findViewById(R.id.btnSum);
        result = (TextView)findViewById(R.id.textViewAnswer);

        addButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                double firstNum  = Double.parseDouble(firstNumberTxt.getText().toString());
                double secoundNum  = Double.parseDouble(secoundNumberTxt.getText().toString());
                result.setText(String.valueOf(firstNum+secoundNum));
            }
        });

    }
}
